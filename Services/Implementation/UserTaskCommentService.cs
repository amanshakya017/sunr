﻿using ApplicationCore.Entities;
using Infrastructure;
using Models.ViewModel;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Services.Implementation
{
    public class UserTaskCommentService : CrudService<UserTaskComments>, IUserTaskCommentService<UserTaskCommentModel>
    {

        public UserTaskCommentService(TestWebApplicatioDbContext context) : base(context)
        {
        }
        public async Task<bool> IsExist(Expression<Func<UserTaskComments, bool>> expression)
        {
            return await base.Exist(expression);
        }
        public async Task<List<UserTaskCommentModel>> Gets()
        {
            List<UserTaskCommentModel> model = new List<UserTaskCommentModel>();
            var dbResponse = await GetAllAsync();
            model = (from d in dbResponse
                     select new UserTaskCommentModel
                     {
                         Id = d.Id,
                         UserTaskId = d.UserTaskId,
                         Comments = d.Comments,
                         IsActive = d.IsActive,
                         CreatedBy = d.CreatedBy,
                         CreatedDate = d.CreatedDate,
                         UpdatedBy = d.UpdatedBy,
                         UpdatedDate = d.UpdatedDate
                     }).ToList();
            return model;
        }
        public async Task<UserTaskCommentModel> Get(int Id)
        {
            var dbResponse = await GetAsync(Id);
            return new UserTaskCommentModel()
            {
                Id = dbResponse.Id,
                UserTaskId = dbResponse.UserTaskId,
                Comments = dbResponse.Comments,
                IsActive = dbResponse.IsActive,
                CreatedBy = dbResponse.CreatedBy,
                CreatedDate = dbResponse.CreatedDate,
                UpdatedBy = dbResponse.UpdatedBy,
                UpdatedDate = dbResponse.UpdatedDate
            };
        }
        public async Task<UserTaskCommentModel> create(UserTaskCommentModel model)
        {
            var data = new UserTaskComments()
            {
                CreatedDate = DateTime.Now,
                UserTaskId = model.UserTaskId,
                Comments = model.Comments,
                IsActive = model.IsActive,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                UpdatedDate = model.UpdatedDate
            };
            var dbResponse = await InsertAsync(data);
            await Save();
            model.Id = dbResponse.Id;
            return model;

        }
        public async Task<UserTaskCommentModel> Update(UserTaskCommentModel model)
        {
            var data = new UserTaskComments()
            {
                Id = model.Id,
                UserTaskId = model.UserTaskId,
                Comments = model.Comments,
                IsActive = model.IsActive,
                CreatedBy = model.CreatedBy,
                UpdatedBy = model.UpdatedBy,
                UpdatedDate = model.UpdatedDate
            };
            var dbResponse = Update(data);
            await Save();
            model.Id = dbResponse.Id;
            return model;

        }
        public new async Task<UserTaskCommentModel> Delete(int id)
        {
            var dbResponse = await base.Delete(id);
            await Save();
            return new UserTaskCommentModel
            {
                Id = dbResponse.Id,
                UserTaskId = dbResponse.UserTaskId,
                Comments = dbResponse.Comments,
                IsActive = dbResponse.IsActive,
                CreatedBy = dbResponse.CreatedBy,
                CreatedDate = dbResponse.CreatedDate,
                UpdatedBy = dbResponse.UpdatedBy,
                UpdatedDate = dbResponse.UpdatedDate
            };
        }
        public async Task<List<SelectListItem>> DDL()
        {
            List<SelectListItem> model = new List<SelectListItem>();
            var dbResponse = await GetAllAsync();
            model = (from d in dbResponse
                     select new SelectListItem
                     {
                         Value = d.Id.ToString(),
                         Text = d.UserTaskId.ToString(),
                     }).ToList();
            return model;
        }

    }
}
