﻿using ApplicationCore.Entities;
using ApplicationCore.Enum;
using Infrastructure;
using Models.ViewModel;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Services.Implementation
{
    public class UserTaskService : CrudService<UserTask>, IUserTaskService<UserTaskModel>
    {

        public UserTaskService(TestWebApplicatioDbContext context) : base(context)
        {
        }
        public async Task<bool> IsExist(Expression<Func<UserTask, bool>> expression)
        {
            return await base.Exist(expression);
        }
        public async Task<List<UserTaskModel>> Gets()
        {
            List<UserTaskModel> model = new List<UserTaskModel>();
            var dbResponse = await GetAllAsync();
            model = (from d in dbResponse
                     select new UserTaskModel
                     {
                         Id = d.Id,
                         TaskId = d.TaskId,
                         TaskStatus = (TaskStatusEnum)Enum.Parse(typeof(TaskStatusEnum), d.TaskStatus),
                         UserId = d.UserId,
                         CreatedBy = d.CreatedBy,
                         CreatedDate = d.CreatedDate,
                         UpdatedBy = d.UpdatedBy,
                         UpdatedDate = d.UpdatedDate
                     }).ToList();
            return model;
        }
      public async Task<UserTaskModel> Get(int Id)
        {
            var dbResponse = await GetAsync(Id);
            return new UserTaskModel()
            {
                Id = dbResponse.Id,
                TaskId = dbResponse.TaskId,
                TaskStatus = (TaskStatusEnum)Enum.Parse(typeof(TaskStatusEnum), dbResponse.TaskStatus),
                UserId = dbResponse.UserId,
            };
        }
        public async Task<UserTaskModel> create(UserTaskModel model)
        {
            var data = new UserTask()
            {
                CreatedDate = DateTime.Now,
                TaskId = model.TaskId,
                TaskStatus = model.TaskStatus.ToString(),
                UserId = model.UserId,
            };
            var dbResponse = await InsertAsync(data);
            await Save();
            model.Id = dbResponse.Id;
            return model;

        }
        public async Task<UserTaskModel> Update(UserTaskModel model)
        {
            var data = await GetAsync(model.Id);
            //var data = new UserTask()
            //{
            data.UpdatedDate = DateTime.Now;
            data.TaskId = model.TaskId;
            data.TaskStatus = model.TaskStatus.ToString();
            data.UserId = model.UserId;
            data.UpdatedBy = model.UpdatedBy;
            data.IsCompletedAfterDeadline = model.IsCompletedAfterDeadline;
            //};
            var dbResponse = Update(data);
            await Save();
            model.Id = dbResponse.Id;
            return model;

        }
        public new async Task<UserTaskModel> Delete(int id)
        {
            var dbResponse = await base.Delete(id);
            await Save();
            return new UserTaskModel
            {
                Id = dbResponse.Id,
                TaskId = dbResponse.TaskId,
                TaskStatus = (TaskStatusEnum)Enum.Parse(typeof(TaskStatusEnum), dbResponse.TaskStatus),
                UserId = dbResponse.UserId,
            };
        }
        public async Task<List<SelectListItem>> DDL()
        {
            List<SelectListItem> model = new List<SelectListItem>();
            var dbResponse = await GetAllAsync();
            model = (from d in dbResponse
                     select new SelectListItem
                     {
                         Value = d.Id.ToString(),
                         Text = d.TaskStatus,
                     }).ToList();
            return model;
        }

    }
}
