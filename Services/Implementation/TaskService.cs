﻿using ApplicationCore.Enum;
using Infrastructure;
using Models.ViewModel;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Services.Implementation
{
    public class TaskService : CrudService<ApplicationCore.Entities.Task>, ITaskService<TaskModel>
    {

        public TaskService(TestWebApplicatioDbContext context) : base(context)
        {
        }
        public async Task<bool> IsExist(Expression<Func<ApplicationCore.Entities.Task, bool>> expression)
        {
            return await base.Exist(expression);
        }
        public async Task<List<TaskModel>> Gets()
        {
            List<TaskModel> model = new List<TaskModel>();
            var dbResponse = await GetAllAsync();
            model = (from d in dbResponse
                     select new TaskModel
                     {
                         Id = d.Id,
                         CreatedDate = d.CreatedDate,
                         CreatedBy = d.CreatedBy,
                         TaskName = d.TaskName,
                         Description = d.Description,
                         Priority = (PriorityEnum)Enum.Parse(typeof(PriorityEnum), d.Priority),
                         StartDate = d.StartDate,
                         EndDate = d.EndDate,
                         IsActive = d.IsActive
                     }).ToList();
            return model;
        }
        public async Task<TaskModel> Get(int Id)
        {
            var dbResponse = await GetAsync(Id);
            PriorityEnum myEnum = (PriorityEnum)Enum.Parse(typeof(PriorityEnum), dbResponse.Priority);
            return new TaskModel
            {
                Id = dbResponse.Id,
                CreatedDate = dbResponse.CreatedDate,
                CreatedBy = dbResponse.CreatedBy,
                TaskName = dbResponse.TaskName,
                Description = dbResponse.Description,
                Priority = myEnum,
                StartDate = dbResponse.StartDate,
                EndDate = dbResponse.EndDate,
                IsActive = dbResponse.IsActive
            };
        }
        public async Task<TaskModel> create(TaskModel model)
        {
            var data = new ApplicationCore.Entities.Task()
            {
                CreatedDate = DateTime.Now,
                CreatedBy = model.CreatedBy,
                TaskName = model.TaskName,
                Description = model.Description,
                Priority = model.Priority.ToString(),
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                IsActive = model.IsActive
            };
            var dbResponse = await InsertAsync(data);
            await Save();
            model.Id = dbResponse.Id;
            return model;

        }
        public async Task<TaskModel> Update(TaskModel model)
        {
            var data = await GetAsync(model.Id);

            data.UpdatedDate= DateTime.Now;
            data.TaskName = model.TaskName;
            data.Description = model.Description;
            data.Priority = model.Priority.ToString();
            data.StartDate = model.StartDate;
            data.EndDate = model.EndDate;
            var dbResponse = Update(data);
            await Save();
            model.Id = dbResponse.Id;
            return model;

        }
        public new async Task<TaskModel> Delete(int id)
        {
            var dbResponse = await base.Delete(id);
            await Save();
            return new TaskModel
            {
                Id = dbResponse.Id,
                TaskName = dbResponse.TaskName
            };
        }
        public async Task<List<SelectListItem>> DDL()
        {
            List<SelectListItem> model = new List<SelectListItem>();
            var dbResponse = await GetAllAsync();
            model = (from d in dbResponse
                     select new SelectListItem
                     {
                         Value = d.Id.ToString(),
                         Text = d.TaskName,
                     }).ToList();
            return model;
        }

    }
}
