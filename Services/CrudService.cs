﻿using ApplicationCore.BaseEntities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CrudService<T> : ICrudService<T> where T : BaseEntity

    {
        public readonly TestWebApplicatioDbContext _context;

        public CrudService(TestWebApplicatioDbContext context)
        {
            _context = context;
        }
        public T Get(int? id)
        {
            return _context.Set<T>().Find(id);
        }

        public T Get(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression).SingleOrDefault();
        }
        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }
        public async Task<T> GetAsync(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().Where(expression).SingleOrDefaultAsync();
        }
        public int Insert(T entity)
        {
            var result = _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return result.Entity.Id;
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().Where(expression).ToListAsync();
        }

        public async Task<T> GetAsync(int? id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task<T> InsertAsync(T entity)
        {
            var sql = await _context.Set<T>().AddAsync(entity);
            //await _context.SaveChangesAsync();
            return sql.Entity;
        }

        public T Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public async Task<T> Delete(int id)
        {
            var entity = await _context.Set<T>().FindAsync(id);
            if (entity == null)
            {
                return entity;
            }
            _context.Set<T>().Remove(entity);
            //await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<bool> Exist(Expression<Func<T, bool>> expression)
        {
            bool entity = await _context.Set<T>().AnyAsync(expression);
            return entity;
        }
        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
        public T OnCreate(T entity, string userEmail)
        {
            entity.CreatedBy = userEmail;
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedDate = DateTime.Now;
            entity.UpdatedBy = userEmail;
            return entity;
        }
        public T OnUpdate(T entity, string userEmail)
        {
            entity.UpdatedDate = DateTime.Now;
            entity.UpdatedBy = userEmail;
            return entity;
        }
    }
}
