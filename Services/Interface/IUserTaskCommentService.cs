﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Services.Interface
{
    public interface IUserTaskCommentService<T>
    {
        Task<List<T>> Gets();
        Task<T> Get(int Id);
        Task<T> create(T model);
        Task<T> Update(T model);
        Task<T> Delete(int id);
        Task<List<SelectListItem>> DDL();
        Task<bool> IsExist(Expression<Func<UserTaskComments, bool>> expression);
    }
}
