﻿using ApplicationCore.Enum;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class TaskModel:CommonModel
    {
        public string TaskName { get; set; }
        public string Description { get; set; }
        [BindProperty, DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [BindProperty, DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        public PriorityEnum Priority { get; set; }
        public bool IsActive { get; set; }
        public string TaskStatus { get; set; }
        public string User { get; set; }

        //public string UserParam { get; set; }
        //[BindProperty, DataType(DataType.Date)]
        //public DateTime? StartDateParam { get; set; }
        //[BindProperty, DataType(DataType.Date)]
        //public DateTime? EndDateParam { get; set; }
        //public PriorityEnum? PriorityParam { get; set; }
        //public TaskStatusEnum? TaskStatusParam { get; set; }
    }
}
