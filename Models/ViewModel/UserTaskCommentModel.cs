﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class UserTaskCommentModel:CommonModel
    {
        public int UserTaskId { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
    }
}
