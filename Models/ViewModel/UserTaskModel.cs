﻿using ApplicationCore.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class UserTaskModel:CommonModel
    {
        public Guid UserId { get; set; }
        public int TaskId { get; set; }
        public TaskStatusEnum TaskStatus { get; set; }
        public bool IsCompletedAfterDeadline { get; set; }

    }
}
