﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Infrastructure.EntityConfiguration;

namespace Infrastructure
{
    public class TestWebApplicatioDbContext: DbContext
    {
        public IConfiguration Configuration { get; }
        public TestWebApplicatioDbContext()
        {

        }
        public TestWebApplicatioDbContext(DbContextOptions<TestWebApplicatioDbContext> options, IConfiguration configuration)
            :base(options)
        {

            Configuration = configuration;
        }
        public DbSet<ApplicationCore.Entities.Task> Task { get; set; }
        public DbSet<ApplicationCore.Entities.UserTask> UserTask { get; set; }
        public DbSet<ApplicationCore.Entities.UserTaskComments> UserTaskComments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conn = Configuration.GetConnectionString("DefaultConnection");
            optionsBuilder
                .UseSqlServer(conn)
                .EnableServiceProviderCaching()
                .EnableDetailedErrors();

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new TaskConfig());
            modelBuilder.ApplyConfiguration(new UserTaskConfig());
            modelBuilder.ApplyConfiguration(new UserTaskCommentConfig());
        }
    }
}
