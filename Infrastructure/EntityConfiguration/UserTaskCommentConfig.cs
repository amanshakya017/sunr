﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityConfiguration
{
    public class UserTaskCommentConfig : IEntityTypeConfiguration<UserTaskComments>
    {
        public void Configure(EntityTypeBuilder<UserTaskComments> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .UseIdentityColumn<int>();
            builder.Property(e => e.UserTaskId).IsRequired();
            builder.Property(e => e.Comments).IsRequired().HasMaxLength(100);
            builder.Property(e => e.IsActive).IsRequired();
        }
    }
}
