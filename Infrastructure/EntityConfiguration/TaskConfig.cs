﻿using ApplicationCore.Entities;
using ApplicationCore.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityConfiguration
{
    public class TaskConfig : IEntityTypeConfiguration<ApplicationCore.Entities.Task>
    {
        public void Configure(EntityTypeBuilder<ApplicationCore.Entities.Task> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
               .UseIdentityColumn<int>();

            builder.Property(e => e.TaskName)
               .IsRequired()
               .HasMaxLength(50);
            builder.Property(e => e.Description)
              .IsRequired()
              .HasMaxLength(50);
            builder.Property(e => e.StartDate)
              .IsRequired();
            builder.Property(e => e.EndDate)
              .IsRequired();
            builder.Property(e => e.Priority)
              .IsRequired().HasDefaultValue(PriorityEnum.Low);
        }
    }
}
