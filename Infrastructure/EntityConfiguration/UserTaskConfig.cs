﻿using ApplicationCore.Entities;
using ApplicationCore.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityConfiguration
{
    public class UserTaskConfig : IEntityTypeConfiguration<UserTask>
    {
        public void Configure(EntityTypeBuilder<UserTask> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .UseIdentityColumn<int>();
            builder.Property(e => e.TaskId).IsRequired();
            builder.Property(e => e.UserId).IsRequired();
            builder.Property(e => e.IsCompletedAfterDeadline).HasDefaultValue(false);
            builder.Property(e => e.TaskStatus).IsRequired().HasDefaultValue(TaskStatusEnum.Pending);
        }
    }
}
