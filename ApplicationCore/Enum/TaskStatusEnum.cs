﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Enum
{
    public enum TaskStatusEnum
    {
        Pending = 1,
        Picked = 2,
        Working = 3,
        Returned = 4,
        Completed = 5
    }
}