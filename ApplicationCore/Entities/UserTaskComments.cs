﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Entities
{
    public class UserTaskComments:BaseEntities.BaseEntity
    {
        [ForeignKey("UserTask")]
        public int UserTaskId { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public UserTask UserTask { get; set; }
    }
}
