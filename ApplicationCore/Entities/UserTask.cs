﻿using ApplicationCore.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Entities
{
    public class UserTask:BaseEntity
    {
        public Guid UserId { get; set; }
        [ForeignKey("Task")] 
        public int TaskId { get; set; }
        public string TaskStatus { get; set; }
        public bool IsCompletedAfterDeadline { get; set; }
        public Task Task { get; set; }
        public ICollection<UserTask> UserTasks { get; set; }
    }
}
