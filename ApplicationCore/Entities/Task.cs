﻿using ApplicationCore.BaseEntities;
using ApplicationCore.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Entities
{
    public class Task: BaseEntity
    {
        public string TaskName { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Priority { get; set; }
        public bool IsActive { get; set; }
        public ICollection<UserTask> Students { get; set; }
    }
}
