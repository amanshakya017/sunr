﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TaskApplicatoin.Web.Models;

namespace TaskApplicatoin.Web.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
        //IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>,
    //ApplicationUserRole, IdentityUserLogin<string>,
    //IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    base.OnModelCreating(builder);

        //    builder.Entity<ApplicationUserRole>(userRole =>
        //    {
        //        userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

        //        userRole.HasOne(ur => ur.Role)
        //            .WithMany(r => r.UserRoles)
        //            .HasForeignKey(ur => ur.RoleId)
        //            .IsRequired();

        //        userRole.HasOne(ur => ur.User)
        //            .WithMany(r => r.UserRoles)
        //            .HasForeignKey(ur => ur.UserId)
        //            .IsRequired();
        //    });
        //}
    }
}
