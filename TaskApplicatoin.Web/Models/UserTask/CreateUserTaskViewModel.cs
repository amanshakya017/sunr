﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskApplicatoin.Web.Models.UserTask
{
    public class CreateUserTaskViewModel
    {
        [Required]
        [Display(Name ="User")]
        public int UserId { get; set; }
        [Required]
        [Display(Name ="Task")]
        public int TaskId { get; set; }
        public string TaskStatus { get; set; }
    }
}
