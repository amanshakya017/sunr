﻿using ApplicationCore.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskApplicatoin.Web.Models.UserTask
{
    public class UserTaskListVM
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public TaskStatusEnum TaskStatus { get; set; }
    }
}
