﻿using ApplicationCore.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskApplicatoin.Web.Models.UserTask
{
    public class UserTaskDetailVM
    {
        public UserTaskDetailVM()
        {
            OldComments = new List<string>();
        }
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Priority { get; set; }
        public TaskStatusEnum TaskStatus { get; set; }
        public string Comment { get; set; }
        public List<string> OldComments { get; set; }
    }
}
