﻿using ApplicationCore.Enum;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TaskApplication.Models.Task
{
    public class CreateTaskViewModel
    {
        public CreateTaskViewModel()
        {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
        [Required]
        public string TaskName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [BindProperty, DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Required]
        [BindProperty, DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        [Required]
        [EnumDataType(typeof(PriorityEnum))]
        public PriorityEnum Priority { get; set; }
        public bool IsActive { get; set; }
        public List<SelectListItem> PriorityList { get; set; }
    }
}
