﻿using ApplicationCore.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TaskApplicatoin.Web.Models.Task
{
    public class AssignUserVM
    {
        public AssignUserVM()
        {
            TaskStatus = TaskStatusEnum.Pending;
        }
        [Required]
        [Display(Name = "User")]
        public Guid UserId { get; set; }
        [Required]
        [Display(Name = "Task")]
        public int TaskId { get; set; }
        public TaskStatusEnum TaskStatus { get; set; }
        public List<SelectListItem> UserList { get; set; }
    }
}
