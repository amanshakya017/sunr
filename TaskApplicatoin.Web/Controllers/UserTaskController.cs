﻿using ApplicationCore.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.ViewModel;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApplicatoin.Web.Models;
using TaskApplicatoin.Web.Models.UserTask;

namespace TaskApplicatoin.Web.Controllers
{
    [Authorize(Roles = "User")]
    //[Authorize]
    public class UserTaskController : Controller
    {
        private readonly IUserTaskService<UserTaskModel> _userTaskService;
        private readonly IUserTaskCommentService<UserTaskCommentModel> _userTaskCommentService;
        private readonly ITaskService<TaskModel> _taskService;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserTaskController(IUserTaskService<UserTaskModel> userTaskService,
            ITaskService<TaskModel> taskService,
            IUserTaskCommentService<UserTaskCommentModel> userTaskCommentService,
            UserManager<ApplicationUser> userManager)
        {
            _userTaskService = userTaskService;
            _taskService = taskService;
            _userManager = userManager;
            _userTaskCommentService = userTaskCommentService;
        }
        public async Task<IActionResult> Index()
        {
            var currentUserId = _userManager.GetUserId(User);
            var userTasks = await _userTaskService.Gets();
            var users = (from user in _userManager.Users
                         select user).ToList();
            var list = (from ut in userTasks
                        join tt in _taskService.Gets().Result on ut.TaskId equals tt.Id
                        join u in users on ut.UserId.ToString() equals u.Id.ToString()
                        where ut.TaskStatus != TaskStatusEnum.Returned && ut.UserId.ToString() == currentUserId
                        select new UserTaskListVM
                        {
                            Id = ut.Id,
                            TaskId = ut.TaskId,
                            UserName = $"{u.FirstName} {u.LastName}",
                            UserId = ut.UserId,
                            TaskStatus = ut.TaskStatus,
                            TaskName = tt.TaskName
                        }).ToList();
            return View(list);
        }
        public async Task<IActionResult> Details(int id)
        {
            var userId = _userManager.GetUserId(User);
            var userTask = await _userTaskService.Get(id);
            if (userTask == null)
            {
                return RedirectToAction("Index");
            }
            var tasks = await _taskService.Gets();
            var model = (from task in tasks
                         where task.Id == userTask.TaskId
                         select new UserTaskDetailVM
                         {
                             Id = userTask.Id,
                             TaskName = task.TaskName,
                             TaskId = userTask.TaskId,
                             Description = task.Description,
                             StartDate = task.StartDate,
                             EndDate = task.EndDate,
                             Priority = task.Priority.ToString(),
                             TaskStatus = userTask.TaskStatus
                         }).FirstOrDefault();
            var oldComments = await _userTaskCommentService.Gets();
            model.OldComments = oldComments.Where(x => x.UserTaskId == model.Id).Select(x => x.Comments).ToList();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Details(int id, UserTaskDetailVM input)
        {
            var userId = _userManager.GetUserId(User);
            try
            {
                if (id == 0)
                {
                    ViewBag.ErrorMessage = "Invalid Request";
                    return View(input);
                }
                var userTask = await _userTaskService.Get(id);
                if (userTask == null)
                {
                    return RedirectToAction("Index");
                }

                var task = await _taskService.Get(userTask.TaskId);
                if (!ModelState.IsValid)
                {
                    ViewBag.ErrorMessage = "Please fill all the require fields.";
                    return View(input);
                }
                userTask.TaskStatus = input.TaskStatus;
                userTask.UpdatedDate = DateTime.Now;
                userTask.UpdatedBy = userId;
                userTask.IsCompletedAfterDeadline = (input.TaskStatus == TaskStatusEnum.Completed && task.EndDate < DateTime.Now) ? true : false;
                UserTaskCommentModel utcModel = new UserTaskCommentModel
                {
                    UserTaskId = id,
                    Comments = input.Comment,
                    IsActive = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = userId
                };
                var utResponse = await _userTaskService.Update(userTask);
                var utcResponse = await _userTaskCommentService.create(utcModel);
                return RedirectToAction("Index", "UserTask");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View(input);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ReturnedTask()
        {
            var currentUserId = _userManager.GetUserId(User);
            var userTasks = await _userTaskService.Gets();
            var users = (from user in _userManager.Users
                         select user).ToList();
            var list = (from ut in userTasks
                        join tt in _taskService.Gets().Result on ut.TaskId equals tt.Id
                        join u in users on ut.UserId.ToString() equals u.Id.ToString()
                        where ut.TaskStatus == TaskStatusEnum.Returned && ut.UserId.ToString() == currentUserId
                        select new UserTaskListVM
                        {
                            Id = ut.Id,
                            TaskId = ut.TaskId,
                            UserName = $"{u.FirstName} {u.LastName}",
                            UserId = ut.UserId,
                            TaskStatus = ut.TaskStatus,
                            TaskName = tt.TaskName
                        }).ToList();
            return View(list);
        }
    }
}
