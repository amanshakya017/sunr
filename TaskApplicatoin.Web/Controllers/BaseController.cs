﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TaskApplicatoin.Web.Models;

namespace TaskApplication.Controllers
{
    public class BaseController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public BaseController()
        {

        }
        public BaseController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        public string GetCurrentUserId()
        {
            var userId = _userManager.GetUserId(User);
            return userId;
        }
    }
}
