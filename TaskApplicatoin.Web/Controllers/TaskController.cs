﻿using ApplicationCore.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.ViewModel;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApplication.Models.Task;
using TaskApplicatoin.Web.Data;
using TaskApplicatoin.Web.Models;
using TaskApplicatoin.Web.Models.Task;

namespace TaskApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    //[Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskService<TaskModel> _taskService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserTaskService<UserTaskModel> _userTaskService;
        private readonly ApplicationDbContext context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public TaskController(
            ITaskService<TaskModel> taskService,
            IUserTaskService<UserTaskModel> userTaskService,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext dbContext,
            SignInManager<ApplicationUser> signInManager
            )
        {
            _taskService = taskService;
            _userManager = userManager;
            _userTaskService = userTaskService;
            context = dbContext;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }

        public async Task<IActionResult> Index(string user, DateTime? startDate, DateTime? endDate, PriorityEnum? priority, TaskStatusEnum? taskStatus)
        {
            var currentUserId = _userManager.GetUserId(User);
            var tasks = await _taskService.Gets();
            var list = await _userTaskService.Gets();
            var tasklist = (from t in tasks
                            join l in list
                            on t.Id equals l.TaskId
                            into g
                            from a in g.DefaultIfEmpty()
                            select new TaskModel
                            {
                                Id = t.Id,
                                TaskName = t.TaskName,
                                Description = t.Description,
                                StartDate = t.StartDate,
                                EndDate = t.EndDate,
                                Priority = t.Priority,
                                IsActive = t.IsActive,
                                TaskStatus = a?.TaskStatus.ToString(),
                            }).ToList();
            tasklist = tasklist.Where(x => x.TaskStatus != TaskStatusEnum.Returned.ToString()).ToList();
            if (!string.IsNullOrEmpty(user))
            {
                tasklist = tasklist.Where(x => x.User.ToLower().Contains(user.ToLower())).ToList();
            }
            if (startDate.HasValue)
            {
                tasklist = tasklist.Where(x => x.StartDate.Date >= startDate.Value.Date).ToList();
            }
            if (endDate.HasValue)
            {
                tasklist = tasklist.Where(x => x.EndDate.Date <= endDate.Value.Date).ToList();
            }
            if (priority.HasValue)
            {
                tasklist = tasklist.Where(x => x.Priority == priority.Value).ToList();
            }
            if (taskStatus.HasValue)
            {
                tasklist = tasklist.Where(x => x.TaskStatus == taskStatus.Value.ToString()).ToList();
            }

            return View(tasklist);
        }
        public async Task<IActionResult> Create()
        {
            var currentUserId = _userManager.GetUserId(User);
            return View(new CreateTaskViewModel());
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateTaskViewModel input)
        {
            var currentUserId = _userManager.GetUserId(User);
            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "Please fill all the require fields.";
                return View(input);
            }
            TaskModel model = new TaskModel
            {
                TaskName = input.TaskName,
                Description = input.Description,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                Priority = input.Priority,
                IsActive = input.IsActive,
                CreatedBy = currentUserId
            };
            var response = await _taskService.create(model);
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> AssignUser(int id)
        {
            var userId = _userManager.GetUserId(User);
            var task = await _taskService.Get(id);
            if (task == null)
            {
                return RedirectToAction("Index");
            }
            var userTasks = await _userTaskService.Gets();
            var userTask = userTasks.Where(x => x.TaskId == id).OrderByDescending(a => a.CreatedDate).FirstOrDefault();
            var roleId = await _roleManager.GetRoleIdAsync(new IdentityRole("User"));
            var users = (from user in _userManager.Users
                         select new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                         {
                             Value = user.Id,
                             Text = $"{user.FirstName} {user.LastName}",
                         }).ToList();
            var model = new AssignUserVM { UserList = users };
            if (userTask != null)
            {
                model.UserId = userTask.UserId;
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AssignUser(int id, AssignUserVM input)
        {
            try
            {
                var roleId = await _roleManager.GetRoleIdAsync(new IdentityRole("User"));
                var users = (from user in _userManager.Users
                             select new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                             {
                                 Value = user.Id,
                                 Text = $"{user.FirstName} {user.LastName}",
                             }).ToList();
                input.UserList = users;
                var userId = _userManager.GetUserId(User);
                if (id == 0)
                {
                    ViewBag.ErrorMessage = "Invalid Request";
                    return View(input);
                }
                var task = await _taskService.Get(id);
                if (task == null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    input.TaskId = task.Id;
                }
                if (!ModelState.IsValid)
                {
                    ViewBag.ErrorMessage = "Please fill all the require fields.";
                    return View(input);
                }
                UserTaskModel model;
                var userTasks = await _userTaskService.Gets();
                model = userTasks.Where(x => x.TaskId == id).OrderByDescending(a => a.CreatedDate).FirstOrDefault();
                if (model == null)
                {
                    model = new UserTaskModel();
                    model.UserId = input.UserId;
                    model.TaskId = input.TaskId;
                    model.TaskStatus = input.TaskStatus;
                    model.CreatedDate = DateTime.Now;
                    model.CreatedBy = userId;
                    var response = await _userTaskService.create(model);
                }
                else
                {
                    model.UserId = input.UserId;
                    model.TaskId = input.TaskId;
                    model.TaskStatus = input.TaskStatus;
                    model.UpdatedDate = DateTime.Now;
                    model.UpdatedBy = userId;
                    var response = await _userTaskService.Update(model);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View(input);
            }
        }

        public async Task<IActionResult> ReturnedTasks()
        {
            var tasks = await _taskService.Gets();
            var list = await _userTaskService.Gets();
            var tasklist = (from t in tasks
                            join l in list
                            on t.Id equals l.TaskId
                            into g
                            from a in g.DefaultIfEmpty()
                            select new TaskModel
                            {
                                Id = t.Id,
                                TaskName = t.TaskName,
                                Description = t.Description,
                                StartDate = t.StartDate,
                                EndDate = t.EndDate,
                                Priority = t.Priority,
                                IsActive = t.IsActive,
                                TaskStatus = a?.TaskStatus.ToString(),
                            }).ToList();
            tasklist = tasklist.Where(x => x.TaskStatus == TaskStatusEnum.Returned.ToString()).ToList();
            return View(tasklist);
        }

        public async Task<IActionResult> DownloadCommaSeperatedFile()
        {
            try
            {
                var tasks = await _taskService.Gets();
                var list = await _userTaskService.Gets();
                var tasklist = (from t in tasks
                                join l in list
                                on t.Id equals l.TaskId
                                into g
                                from a in g.DefaultIfEmpty()
                                select new
                                {
                                    Id = t.Id,
                                    TaskName = t.TaskName,
                                    Description = t.Description,
                                    StartDate = t.StartDate,
                                    EndDate = t.EndDate,
                                    Priority = t.Priority,
                                    IsActive = t.IsActive,
                                    TaskStatus = a?.TaskStatus.ToString(),
                                    IsCompletedAfterDeadline = a?.IsCompletedAfterDeadline
                                }).ToList();
                tasklist = tasklist.Where(x => x.TaskStatus != TaskStatusEnum.Returned.ToString()).ToList();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Id,Task Name, Description,Start Date,End Date,Priority,Task Status,IsCompletedAfterDeadline");
                foreach (var item in tasklist)
                {
                    stringBuilder.AppendLine($"{item.Id}, { item.TaskName},{ item.Description},{ item.StartDate},{ item.EndDate},{ item.Priority},{ item.TaskStatus},{item.IsCompletedAfterDeadline} ");
                }
                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "Task_Report.csv");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Index");
            }
        }
        public async Task<IActionResult> Edit(int id)
        {
            var userId = _userManager.GetUserId(User);
            var task = await _taskService.Get(id);
            if (task == null)
            {
                return RedirectToAction("Index");
            }
            return View(task);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int id, TaskModel input)
        {
            try
            {
                var userId = _userManager.GetUserId(User);
                if (id == 0)
                {
                    ViewBag.ErrorMessage = "Invalid Request";
                    return View(input);
                }

                var taskResponse = await _taskService.Update(input);
                UserTaskModel model;
                var userTasks = await _userTaskService.Gets();
                if (userTasks.Any())
                {
                    model = userTasks.Where(x => x.TaskId == id).OrderByDescending(a => a.CreatedDate).FirstOrDefault();
                    model.TaskStatus = TaskStatusEnum.Pending;
                    model.UpdatedDate = DateTime.Now;
                    model.UpdatedBy = userId;
                    var response = await _userTaskService.Update(model);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View(input);
            }
        }

    }
}
